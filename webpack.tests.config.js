const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    context: path.join( __dirname ,'tests' ),
    entry: [
        path.join( __dirname ,'tests/index.js' ),
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:3000'
    ],
    output: {
        path: path.join( __dirname , 'dist' ),
        publicPath: '/dist/',
        filename: 'test.bulid.js',
        hotUpdateChunkFilename: 'hot/[id].[hash].hot-update.js',
        hotUpdateMainFilename: 'hot/[hash].hot-update.json'
    },
    resolve: {
        extensions: [ '.js', '.jsx' ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    ],

    // różne rodzaje modułu do odpowiedniego formatu
    module: {
        rules: [
            {
              test: /\.jsx?$/, //js, jsx
              loaders: [ 'babel-loader' ],
              include: path.join( __dirname, 'src' ),
              exclude: [
                 path.resolve(__dirname, "node_modules")
              ]
            }
        ]
    }
}
