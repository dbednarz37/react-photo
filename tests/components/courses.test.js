import * as React from 'react'
import expect from 'expect'
import {renderIntoDocument} from 'react-addons-test-utils'
import Courses from '../../src/js/components/courses'

describe('js/components', () => {
  it('should render', () => {
    const item = renderIntoDocument(
      <Courses />
    )
    //assertion
    expect(item).toExist()
  })
})
