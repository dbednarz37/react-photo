import * as React from 'react'
import expect from 'expect'
import {renderIntoDocument} from 'react-addons-test-utils'
import Favorites from '../../src/js/components/favorites'

describe('js/components', () => {
  it('should render', () => {
    const item = renderIntoDocument(
      <Favorites />
    )
    //assertion
    expect(item).toExist()
  })
})
