const express = require("express");
const path = require("path");
const webpack = require('webpack');
const webpackDevServer = require('webpack-dev-server');
const webpackConfig = require('./webpack.config');


const compiler = webpack( webpackConfig );
//const app = express();

const app = new webpackDevServer( compiler, {
   hot: true,
   filename: webpackConfig.output.filename,
   publicPath: webpackConfig.output.publicPath,
   stats: {
     colors: true
   }
})

app.use("/dist", express.static(__dirname + "/dist"));

/*
app.get( '/', function( req, res ) {
//  res.send("Hello World");
  res.sendFile(path.join(__dirname + '/index.html'));
})
*/

app.listen( '3000', 'localhost', function() {
  console.log("App listening on port 3000!!!");
})
