import React from 'react'
import {Link} from 'react-router'

export default class Nav extends React.Component {
  render () {
    return (
      <div>
        <h2>NAV</h2>
        <ul>
          <li><Link className='test' activeClassName='active' to='/'>Mainpage</Link></li>
          <li><Link activeClassName='active' to='/kursy'>Kursy</Link></li>
          <li><Link activeClassName='active' to='/ulubione'>Ulubione</Link></li>
          <li><Link activeClassName='active' to='/koszyk'>Koszyk</Link></li>
        </ul>
      </div>
    )
  }
}
