import React from 'react'

import Header from '../partials/header'
import Footer from '../partials/footer'
import Nav from '../partials/nav'

export default class Layout extends React.Component {
  render () {
    return (
      <div>
        <Header />
        <Nav />
        {this.props.children}
        <Footer />
      </div>
    )
  }
}
