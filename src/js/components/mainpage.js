import React from 'react'

export default class Mainpage extends React.Component {
  constructor (props) {
    super(props)
    console.log('get Initila State')
    console.log(props)
    this.state = {
      name: 'React',
      version: '1.0.0'
    }
  }
  handleClick () {
    console.log('handleClick')
    this.setState({
      version: '1.0.1'
    })
  }
  render () {
    return (
      <div>
        <h1>Mainpage</h1>
        <p onClick={this.handleClick.bind(this)}>To jest test { this.state.name } w wersji { this.state.version }!</p>
      </div>
    )
  }
}
