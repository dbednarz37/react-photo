import React from 'react';

import {Router, Route, browserHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux'

import Layout from './components/layout';

import Mainpage from './components/mainpage';
import Courses from './components/courses';
import Cart from './components/cart';
import Favorites from './components/favorites';
import NotFound from './components/notFound';


class Root extends React.Component {
  render() {
    return ( 
       <div>
       <Router history={history}>
        <Route path="/" component={Layout} >
          <IndexRoute component={Mainpage} />
          <Route path="kursy" >
            <IndexRoute component={Courses} />
            <Route path=":id" component={Cart} />
          </Route>
          <Route path="koszyk" component={Cart} />
          <Route path="ulubione" component={Favorites} />
        </Route>
        <Route path="*" component={NotFound} />
        </div>
        </Router>
        )

export default RouterComponent;
