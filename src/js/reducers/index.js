import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import productReducer from './ProductsReducer'
import testReducer from './TestReducer'

const combineReducersVar = combineReducers({
  products: productReducer,
  tweet: testReducer,
  routing: routerReducer
})

export default combineReducersVar
