import { applyMiddleware, createStore, compose } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'

import DevTools from './devTools'
import reducers from './reducers'

const error = (store) => (next) => (action) => {
  try {
    next(action)
  } catch (e) {
    console.log(e)
  }
}

const middleware = compose(
  applyMiddleware(createLogger(), thunk, error),
  DevTools.instrument()
)
console.log(middleware)

export default createStore(reducers, {}, middleware)
