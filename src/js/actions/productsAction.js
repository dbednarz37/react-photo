export function fetchProducts () {
  return {
    type: 'FETCH_PRODUCTS',
    payload: {
      0: 'product1',
      1: 'product2'
    }
  }
}
