// https://github.com/reactjs/react-router-redux

import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import { Provider } from 'react-redux'
import store from './store'
import DevTools from './devTools'

import { fetchProducts } from './actions/productsAction'

import Layout from './components/layouts/layout'

import Mainpage from './components/mainpage'
import Courses from './components/courses'
import Cart from './components/cart'
import Favorites from './components/favorites'
// import NotFound from './components/notFound'

const history = syncHistoryWithStore(browserHistory, store)
// history.listen(location => analyticsService.track(location.pathname))
// <Route path="*" component={NotFound} />

class RootComponent extends React.Component {
  render () {
    return (
      <div>
        <Provider store={store}>
          <Router history={history}>
            <Route path='/' component={Layout} store={store} >
              <IndexRoute component={Mainpage} />
              <Route path='kursy' >
                <IndexRoute component={Courses} />
                <Route path=':id' component={Cart} />
              </Route>
              <Route path='koszyk' component={Cart} />
              <Route path='ulubione' component={Favorites} />
            </Route>
          </Router>
        </Provider>
      </div>
    )
  }
}

class DevToolsComponent extends React.Component {
  render () {
    return (
      <div>
        <Provider store={store}>
          <DevTools store={store} />
        </Provider>
      </div>
    )
  }
}

ReactDOM.render(<RootComponent />, document.getElementById('root'))
ReactDOM.render(<DevToolsComponent />, document.getElementById('devtools'))

store.subscribe(() => {
  console.log('subscrie 322 dd sasassasaddssd')
  console.log(store)
  console.log(store.getState())
})

store.dispatch(fetchProducts())
/*
store.dispatch( {
    type: 'TEST',
    payload: 'Test-payload'
});
store.dispatch( {
    type: 'TEST2',
    payload: 'Test-payload'
});
store.dispatch( {
    type: 'TEST30',
    payload: 'Test-payload'
});
*/
