const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: [
        path.join( __dirname ,'src/js/main.js' ),
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:3000'
    ],
    plugins: [
      new HtmlWebpackPlugin({
        version: '?version=1',
        template: path.join( __dirname ,'index2.html' ),
        inject: 'body'
      }),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    ],
    output: {
        path: path.join( __dirname , 'dist' ),
        publicPath: '/dist/',
        filename: 'bundle.js',
        hotUpdateChunkFilename: 'hot/[id].[hash].hot-update.js',
        hotUpdateMainFilename: 'hot/[hash].hot-update.json'
    },
    resolve: {
        extensions: [ '.js', '.jsx' ]
    },

/*
    plugins: [
      new ExtractTextPlugin({
        filename: '[name].[id].style.css',
        allChunks: true
      })
    ],
    */

    // różne rodzaje modułu do odpowiedniego formatu
    module: {
        rules: [
            {
              enforce: 'pre',
              test: /\.jsx?$/, //js, jsx
              loaders: [ 'standard-loader' ],
              include: path.join( __dirname, 'src' ),
              exclude: [
                 path.resolve(__dirname, "node_modules")
              ]
            },
            {
              test: /\.jsx?$/, //js, jsx
              loaders: [ 'babel-loader' ],
              include: path.join( __dirname, 'src' ),
              exclude: [
                 path.resolve(__dirname, "node_modules")
              ]
            },
            /*
            {
              test: /\.scss$/,
              loader: ExtractTextPlugin.extract({
                 fallbackLoader: 'style-loader',
                 loader: 'css-loader!less-loader'
              })
            }
            */
        ]
    },


/*
    plugins: [
      new ExtractTextWebpackPlugin("app.css")
    ],
    */

    watch: true
}
